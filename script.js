const agregarTarea = document.querySelector('.agregarTarea');
const misTareas = document.querySelector('.misTareas');
const tareas= JSON.parse(localStorage.getItem('tasklist')) ||[];
imprimirTareas();
agregarTarea.addEventListener('submit', agregar);
misTareas.addEventListener('click', finalizadas);

function agregar(e){
    e.preventDefault();
    const textoTarea = this.querySelector('[name=tareas]').value;
    const tarea = {
        textoTarea, realizada:false
    }
//console.log(this);
tareas.push(tarea);
GuardarAlmacenamientoLocal();
imprimirTareas();
//console.log(tarea);
//console.log(tareas);
this.reset();
}

function GuardarAlmacenamientoLocal(){
    localStorage.setItem('tasklist', JSON.stringify( tareas));
}

function imprimirTareas(){
    let html = tareas.map(function (data, i){
        let myClass = data.realizada ? ' realizada ':'';
        console.log(myClass);
        return '<li><div data-index='+i+' class='+myClass+'>'+data.textoTarea+'</div></li>';
    })
    misTareas.innerHTML = html.join('');
}

function finalizadas(e){
    const myEl = e.target;
    const indice = myEl.dataset.index;
    myEl.classList.toggle('realizada');
    tareas[indice].realizada = !tareas[indice].realizada;
    GuardarAlmacenamientoLocal();
    //imprimirTareas();
console.log(tareas);
//console.log(e);
}